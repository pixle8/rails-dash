Rails.application.routes.draw do
  resources :companies
  resources :projects
  get 'home/index'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
  

  resources :users do
    resources :projects do
    end
  end
end
