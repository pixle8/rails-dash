class User < ApplicationRecord
  rolify :role_cname => 'Client'
  rolify :role_cname => 'Employee'
  rolify :role_cname => 'Admin'
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

         has_many :projects
  after_create :assign_default_role

  def assign_default_role
    self.add_role(:client) if self.roles.blank?
  end
end
