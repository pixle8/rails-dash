class RolifyCreateEmployees < ActiveRecord::Migration
  def change
    create_table(:employees) do |t|
      t.string :name
      t.references :resource, :polymorphic => true

      t.timestamps
    end

    create_table(:users_employees, :id => false) do |t|
      t.references :user
      t.references :employee
    end

    add_index(:employees, :name)
    add_index(:employees, [ :name, :resource_type, :resource_id ])
    add_index(:users_employees, [ :user_id, :employee_id ])
  end
end
